import numpy as np

N_EVALUATIONS = 4
N_JOBS = -1
SEED = 122222

class Optimiser(object):
  def __init__(self, x0, lr=1e-1, h=1e-2):
    self.x0 = x0
    self.ndim = len(x0)

    self.x = np.copy(x0)
    self.f = None

    self.h = h
    self.lr = lr

    self.grad = list()

  def ask(self, ):
    ### value of the function in the current guess is unknown
    if self.f is None:
      return self.x0

    elif len(self.grad) < self.ndim:
      dx = np.zeros(self.ndim)
      dx[len(self.grad)] = self.h

      return self.x + dx
    else:
      raise ValueError()

  def tell(self, x, f):
    if self.f is None:
      self.f = f
    else:
      grad_i = (f - self.f) / self.h
      self.grad.append(grad_i)

      if len(self.grad) >= self.ndim:
        grad = np.array(self.grad)[:self.ndim]
        self.x -= self.lr * grad

        ### resetting f and grad
        self.f = None
        self.grad = list()
      else:
        pass

  def reset(self, ):
    self.x = self.x0
    self.f = None

if __name__ == '__main__':
  from tqdm import tqdm

  from fel import SASE, random_beam, random_geometry
  from fel import eval_optimization

  def eval(sase, id):
    epsilon = 1e-12
    objective = lambda x: np.log(1e-3) - np.log(sase.rho_int(x) + epsilon)

    bounds = np.stack([
      -2 * np.ones(sase.ndim()),
      +2 * np.ones(sase.ndim())
    ], axis=1)

    x0 = np.zeros(sase.ndim())
    f0 = objective(x0)

    xs, fs = eval_optimization(
      Optimiser,
      bounds=bounds,
      f=objective,
      x0=x0,
      moving_cost=1,
      measuring_cost=1,
      budget=128,
      progress=lambda *args, **kwargs: tqdm(*args, **kwargs, position=id)
    )

    return f0, xs, fs


  import joblib
  rng = np.random.RandomState(seed=SEED)

  eval_results = joblib.Parallel(n_jobs=N_JOBS, )(
    joblib.delayed(eval)(SASE(random_beam(rng), random_geometry(rng)), id=i)
    for i in range(N_EVALUATIONS)
  )

  print()
  for i, (f0, xs, fs) in enumerate(eval_results):
    print('Run %d' % (i, ))
    print('  Improvement: %.3lf' % (f0 - np.min(fs), ))

  scores = [
    np.min(fs)
    for f0, xs, fs in eval_results
  ]

  print('Total:')
  print('  Avg. score:', np.mean(scores))
  print('  std:', np.std(scores))

